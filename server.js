const express = require('express');
const mongoose = require('mongoose');
const graphqlHTTP = require('express-graphql');
const app = express();
const schema = require('./Schema/schema');


const connectDB = async () => {
    const conn = await mongoose.connect('mongodb+srv://sangam:sangam123@cluster0-vf4rr.mongodb.net/books?retryWrites=true&w=majority', {
        useCreateIndex: true,
        useFindAndModify: false,
        useNewUrlParser: true,
        useUnifiedTopology: true,

    });

    if (conn) {
        console.log('Database connection successfull!!')
    } else {
        console.log('Database connection failed!!!')
    }
}
//connection with mongodb
connectDB()

//register single query there is one endpoint

app.use('/graphql', graphqlHTTP({
    schema,
    graphiql: true
}))


app.listen(3000, () => {
    console.log(`server running on port 3000...`)
})